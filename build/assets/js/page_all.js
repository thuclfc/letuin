/*!
 * 
 * 
 * 
 * @author Thuclfc
 * @version 
 * Copyright 2021. MIT licensed.
 */$(document).ready(function () {
  //scroll item
  function scrollbar(item, element) {
    var scroll_item_length = $(item).length;
    var scroll_item_width = $(item).outerWidth() + 30;
    $(element).css('min-width', scroll_item_length * scroll_item_width - 30);
  }

  $(window).on('resize scroll load', function () {
    scrollbar('.lesson__list .item', '.lesson__list');
    scrollbar('.lessonfree__list .item', '.lessonfree__list');
    scrollbar('.review__list .item', '.review__list');
    scrollbar('.partner__list li', '.partner__list');
    scrollbar('.practice__list .item', '.practice__list');
    scrollbar('.book__list .item', '.book__list');
  }); //function collapse

  function collapse(btn, item) {
    $(btn).on('click', function (e) {
      e.preventDefault();
      $(this).toggleClass('toggle');
      $(item).slideToggle();
      return false;
    });
  }
  /* collapse('.classroom .btn_collapse','.classroom .collapse');*/

  /*Accordion*/


  function accordion(acc_content, acc_btn, acc_item, acc_active) {
    var menu_ul = $(acc_content);
    var menu_li = $(acc_btn);
    var menu_li_active = $(acc_active);
    menu_ul.hide();
    menu_li_active.find('.menu,.item__content,.accordion__content').slideToggle();
    menu_li.click(function (e) {
      e.preventDefault();
      $(this).parents(acc_item).toggleClass('show');

      if (menu_ul.parent('.active')) {
        $(this).parents(acc_item).find(menu_ul).slideToggle();
      }

      return false;
    });
  }
  /*accordion('.classroom .item__content','.classroom .btn_collapse','.classroom .item');*/


  accordion('.accordion__content', '.accordion__header', '.accordion__item', '.show');
  accordion('.menu-accordion .menu', '.menu-accordion .btn_collapse', '.menu-accordion .item', '.menu-accordion .show');
  accordion('.notice .notice__content', '.notice .btn_collapse', '.notice');
  accordion('.search__acordion .item__content', '.search__acordion .item__header', '.search__acordion .item', '.search__acordion .show');
  $('.expand_all').on('click', function () {
    $('.toggle-accordion').addClass('expanded');
    $('.menu-accordion .item').addClass('show');
    $('.menu-accordion .show').find('.menu').slideDown();
  });
  $('.colpand_all').on('click', function () {
    $('.toggle-accordion').removeClass('expanded');
    $('.menu-accordion .item').removeClass('show').find('.menu').slideUp();
  });

  function active_btn(item, item_active) {
    $(item).on('input', function () {
      if ($(this).val().length > 0) {
        $(item_active).addClass('active');
      } else {
        $(item_active).removeClass('active');
      }
    });
  }

  active_btn('.widthdrawal input[type="password"]', '.widthdrawal .btn_submit'); //close element

  $('.box-text .close,.txt-notifi .close,.box-note .close,.keyword__list .close').on('click', function () {
    $(this).parent().css('display', 'none');
  });
  $('.icon-i').on('click', function () {
    $('.box-text').css('display', 'block');
  }); //name file

  $("#file").change(function () {
    filename = this.files[0].name;
    $('.attachment__name').html(filename);
    $(this).parent().addClass('selected');
  });
  $('.attachment .btn_remove').on('click', function () {
    $('#file').val('');
    $('.attachment__name').html('');
    $(this).parent().removeClass('selected');
  });
  $('.request__list a').on('click', function () {
    $('.request__list a').removeClass('active');
    $(this).addClass('active');
  }); //active button

  $('.modal-password input[type="password"]').on('input', function () {
    if ($(this).val().length > 0) {
      $('.modal-password .btn_submit').addClass('active');
    } else {
      $('.modal-password .btn_submit').removeClass('active');
    }
  }); //show hide button backtop

  $(window).scroll(function () {
    if ($(this).scrollTop() > 200) {
      $('.backtop').fadeIn();
    } else {
      $('.backtop').fadeOut();
    }
  });
  $('.backtop').on('click', function () {
    $('html,body').animate({
      scrollTop: 0
    }, 300);
  }); //scroll left item

  $('.item__scroll').on('scroll', function () {
    var rect = $(this).find('.content').offset().left;

    if (rect < -30) {
      $(this).removeClass('right').addClass('left');
    } else if (rect > 0) {
      $(this).removeClass('left').addClass('right');
    }
  }); //show category

  $('.category .btn_collapse').on('click', function () {
    $('.category').toggleClass('show');
  }); //close tag search

  $('.search_recent .btn_close').on('click', function () {
    $(this).parent().remove();
  });
  $('.search_recent .btn_remove').on('click', function () {
    $('.search_recent .list').remove();
  }); //show follow

  $('.follow .btn_collapse').on('click', function () {
    $(this).parents('.follow').toggleClass('show');
  }); //show keyword

  $('.keyword__toggle').on('click', function () {
    $(this).parent().toggleClass('show');
  }); //input checked

  $('.cart .checkbox__all').on('change load', function () {
    if ($(this).is(':checked')) {
      $('.cart__list input[type="checkbox"]').prop("checked", true);
    } else {
      $(this).parents('li').removeClass('active');
      $('.cart__list input[type="checkbox"]').prop("checked", false);
    }
  });
  $('.btn_remove').on('click', function () {
    $('.cart__list .item').each(function () {
      $('.cart__list .item').find('input:checked').parents('.item').remove();
    });
  });
  $('.modal-filter .check__all_filter').on('change load', function () {
    if ($(this).is(':checked')) {
      $('.checkbox_list input[type="checkbox"]').prop("checked", true);
    } else {
      $(this).parents('li').removeClass('active');
      $('.checkbox_list input[type="checkbox"]').prop("checked", false);
    }
  });
  $('.modal-filter .checkbox_list input[type="checkbox"]').on('change load', function () {
    if ($(this).is(':checked')) {
      $('.modal-filter .check__all_filter').prop("checked", false);
    } else {
      $('.modal-filter .check__all_filter').prop("checked", false);
    }
  });
  $('.rules .check_all_list').on('change load', function () {
    if ($(this).is(':checked')) {
      $('.rules input[type="checkbox"]').prop("checked", true);
    } else {
      $(this).parents('li').removeClass('active');
      $('.rules input[type="checkbox"]').prop("checked", false);
    }
  });
  $('.rules input[type="checkbox"]').on('change load', function () {
    if ($(this).is(':checked')) {
      $('.rules .check_all_list').prop("checked", true);
    } else {
      $('.rules .check_all_list').prop("checked", false);
    }
  });
  $('.modal-addinfo .select_list input').on('change load', function () {
    if ($('.modal-addinfo .select_list input').is(':checked')) {
      $('.btn_next_com').addClass('active');
    } else {
      $('.btn_next_com').removeClass('active');
    }
  }); //show hide filter

  $('.filter .btn_viewmore').on('click', function () {
    $('.filter_sub').toggleClass('show');
    $(this).css('display', 'none');
  });
  $('.coupon__box input').on('click', function () {
    $(this).val('');
  }); //set time coutdown

  function timer() {
    var time_now = new Date();
    time_now = Date.parse(time_now) / 1000;
    var endtime = new Date("25 Mar 2021 00:00:00");
    endtime = Date.parse(endtime) / 1000;
    var time_left = endtime - time_now;
    var days = Math.floor(time_left / 86400);
    var hours = Math.floor((time_left - days * 86400) / 3600);
    var minutes = Math.floor((time_left - days * 86400 - hours * 3600) / 60);
    var seconds = Math.floor(time_left - days * 86400 - hours * 3600 - minutes * 60);

    if (hours < "10") {
      hours = "0" + hours;
    }

    if (minutes < "10") {
      minutes = "0" + minutes;
    }

    if (seconds < "10") {
      seconds = "0" + seconds;
    }

    if (time_left <= 0) {
      days = '00';
      hours = '00';
      minutes = '00';
      seconds = '00';
    }

    $(".days .time").html(days);
    $(".hours .time").html(hours);
    $(".minutes .time").html(minutes);
    $(".seconds .time").html(seconds);
  }

  setInterval(function () {
    timer();
  }, 1000);
}); //scroll effect

$.fn.isInViewport = function () {
  var elementTop = $(this).offset().top;
  var elementBottom = elementTop + $(this).outerHeight() - 100;
  var viewportTop = $(window).scrollTop();
  var viewportBottom = viewportTop + $(window).height() - 100;
  return elementBottom > viewportTop && elementTop < viewportBottom;
};

$(window).on('resize scroll load', function () {
  $('.fadeup').each(function () {
    if ($(this).isInViewport()) {
      $(this).addClass('fadeInUp').css({
        'opacity': '1',
        'visibility': 'visible'
      });
    }
  });
  $('.fadein').each(function () {
    if ($(this).isInViewport()) {
      $(this).addClass('fadeIn').css({
        'opacity': '1',
        'visibility': 'visible'
      });
    }
  });
  $('.zoomin').each(function () {
    if ($(this).isInViewport()) {
      $(this).addClass('zoomIn').css({
        'opacity': '1',
        'visibility': 'visible'
      });
    }
  });
  $('.fadeinleft').each(function () {
    if ($(this).isInViewport()) {
      $(this).addClass('fadeInLeft').css({
        'opacity': '1',
        'visibility': 'visible'
      });
    }
  });
  $('.fadeinright').each(function () {
    if ($(this).isInViewport()) {
      $(this).addClass('fadeInRight').css({
        'opacity': '1',
        'visibility': 'visible'
      });
    }
  });
});